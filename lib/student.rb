class Student

  require 'byebug'

  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    first_name + " " + last_name
  end

  def enroll(course)
    raise "That course has a conflict with an existing course" if @courses.any? { |e| e.conflicts_with?(course) }
    @courses << course unless @courses.include?(course)
    course.students << self
  end

  def course_load
    load = Hash.new(0)
    @courses.each { |course| load[course.department] += course.credits }
    load
  end
end
