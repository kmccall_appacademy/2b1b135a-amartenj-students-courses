class Course

  require 'byebug'

  attr_accessor :name
  attr_accessor :department
  attr_accessor :credits
  attr_accessor :days
  attr_accessor :time_block

  attr_accessor :students

  def initialize(name, department, credits, days, time_block)
    @name = name
    @department = department
    @credits = credits
    @days = days
    @time_block = time_block

    @students = []
  end

  def conflicts_with?(course)
    # debugger
    (not (@days & course.days).empty?) && @time_block == course.time_block
  end

  def add_student(student)
    student.enroll(self) unless @students.include?(student)
    # @students << student unless @students.include?(student)
  end
end
